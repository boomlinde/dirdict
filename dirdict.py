import os
import pickle

class DirDict:
    def __init__(self, path, mode=0700):
        self.path = os.path.expanduser(path)
        if not os.path.exists(self.path):
            os.makedirs(self.path, mode)

    def __contains__(self, key):
        return key in os.listdir(self.path)

    def __len__(self):
        return len(os.listdir(self.path))

    def __setitem__(self, key, value):
        pickle.dump(value, file(os.path.join(self.path, key), 'w+b'))

    def __getitem__(self, key):
        return pickle.load(file(os.path.join(self.path, key)))

    def __delitem__(self, key):
        os.unlink(os.path.join(self.path, key))

    def keys(self):
        return os.listdir(self.path)

    def clear(self):
        for f in os.listdir(self.path):
            os.unlink(os.path.join(self.path, f))

    def get(key, default=None):
        try:
            return self.__getitem__(key)
        except IOError:
            return default

if __name__ == '__main__':
    a = DirDict("~/ddtest")
    a['testing'] = "asdf"
    print a['testing']
